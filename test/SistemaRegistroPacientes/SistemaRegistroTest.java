package SistemaRegistroPacientes;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;

public class SistemaRegistroTest {
    private BaseDeDatosPacientes baseDatosPacientes;
    private BaseDeDatosMedicos baseDatosMedicos;
    private GestorCitas gestorCitas;

    @BeforeEach
    void setUp() {
        baseDatosPacientes = new BaseDeDatosPacientes();
        baseDatosMedicos = new BaseDeDatosMedicos();
        gestorCitas = new GestorCitas();

        Paciente paciente1 = new Paciente("Amelia", "Rodrigez", "Femenino", "Sura Vida", "Hirstorial medico de Amelia", 34, "1023435876", 182324);
        Paciente paciente2 = new Paciente("David", "Lopez", "Masculino", "Salud Vida", "Hirstorial medico de David", 54, "74243546", 1125567);
        baseDatosPacientes.agregarPaciente(paciente1);
        baseDatosPacientes.agregarPaciente(paciente2);

        Medico medico1 = new Medico("M01", "Dr. Juan", "Cardiologia", "Lunes y Jueves");
        Medico medico2 = new Medico("M02", "Dra. Angela", "Pediatria", "Martes y Viernes");

        baseDatosMedicos.agregarMedico(medico1);
        baseDatosMedicos.agregarMedico(medico2);

        Cita cita1 = new Cita("C01", "1023435876", "M01", "2024-01-25 10:00");
        Cita cita2 = new Cita("C02", "1023556768", "M02", "2024-01-26 11:00");
        gestorCitas.crearCita(cita1);
        gestorCitas.crearCita(cita2);
    }

    @Test
    void testAgregarYBuscarPaciente() {
        Paciente paciente3 = new Paciente("Jacinto", "De Luca", "Masculino", "Sura Vida", "Hirstorial medico de Jacinto", 26, "1023556768", 182336);
        baseDatosPacientes.agregarPaciente(paciente3);

        Assertions.assertEquals(paciente3, baseDatosPacientes.buscarPaciente("1023556768"));
    }

    @Test
    void testAgregarYBuscarMedico() {
        Medico medico3 = new Medico("M03", "Dr. Roberto", "Neurologia", "Miercoles y Viernes");
        baseDatosMedicos.agregarMedico(medico3);

        Assertions.assertEquals(medico3, baseDatosMedicos.buscarMedico("M03"));
    }

    @Test
    void testCrearYBuscarCita() {
        Cita cita = new Cita("C03", "1023556768", "M01", "2024-01-27 14:00");
        gestorCitas.crearCita(cita);

        Assertions.assertEquals(cita, gestorCitas.buscarCita("C03"));
    }

    @Test
    void testCancelarCita() {
        Cita cita = new Cita("C02", "1023435876", "M02", "2024-01-26 11:00");
        gestorCitas.crearCita(cita);
        gestorCitas.cancelarCita("C02");

        Assertions.assertNull(gestorCitas.buscarCita("C02"));
    }

}