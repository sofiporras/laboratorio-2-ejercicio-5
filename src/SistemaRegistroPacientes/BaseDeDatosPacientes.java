package SistemaRegistroPacientes;
import java.util.ArrayList;

public class BaseDeDatosPacientes {
    private ArrayList<Paciente> pacientes;

    public BaseDeDatosPacientes() {
        this.pacientes = new ArrayList<>();
    }

    public void agregarPaciente(Paciente pacientes) {
        this.pacientes.add(pacientes);
        System.out.println("Paciente agregado: " + pacientes.getNombre());
    }

    public Paciente buscarPaciente(String dni) {
        for (Paciente paciente : pacientes) {
            if (paciente.getDni().equals(dni)) {
                return paciente;
            }
        }
        return null;
    }

    public void actualizarPaciente(Paciente pacientes) {
        for (int i = 0; i < this.pacientes.size(); i++) {
            if (this.pacientes.get(i).getDni().equals(pacientes.getDni())) {
                this.pacientes.set(i, pacientes);
                System.out.println("Paciente actualizado: " + pacientes.getNombre());
                return;
            }
        }
    }
}
