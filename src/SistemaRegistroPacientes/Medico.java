package SistemaRegistroPacientes;

public class Medico {
    private String dni, nombre, especialidad, horarioDisponible;

    public Medico(String dni, String nombre, String especialidad, String horarioDisponible) {
        this.dni = dni;
        this.nombre = nombre;
        this.especialidad = especialidad;
        this.horarioDisponible = horarioDisponible;
    }

    //getters and setters


    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getHorarioDisponible() {
        return horarioDisponible;
    }

    public void setHorarioDisponible(String horarioDisponible) {
        this.horarioDisponible = horarioDisponible;
    }

    //actualizar disponibilidad
    public void actualizarDisponibilidad(String nuevosHorarios) {
        this.horarioDisponible = nuevosHorarios;
        System.out.println("Disponibilidad del médico " + nombre + " actualizada.");
    }
}
