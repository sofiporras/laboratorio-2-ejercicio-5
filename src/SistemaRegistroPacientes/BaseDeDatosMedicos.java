package SistemaRegistroPacientes;
import java.util.ArrayList;

public class BaseDeDatosMedicos {
    private ArrayList<Medico> medicos;

    public BaseDeDatosMedicos() {
        this.medicos = new ArrayList<>();
    }

    public void agregarMedico(Medico medico) {
        medicos.add(medico);
        System.out.println("Médico agregado: " + medico.getNombre());
    }

    public Medico buscarMedico(String dni) {
        for (Medico medico : medicos) {
            if (medico.getDni().equals(dni)) {
                return medico;
            }
        }
        return null;
    }

    public ArrayList<Medico> listarMedicosPorEspecialidad(String especialidad) {
        ArrayList<Medico> medicosEspecialidad = new ArrayList<>();
        for (Medico medico : medicos) {
            if (medico.getEspecialidad().equals(especialidad)) {
                medicosEspecialidad.add(medico);
            }
        }
        return medicosEspecialidad;
    }
}
