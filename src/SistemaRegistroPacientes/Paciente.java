package SistemaRegistroPacientes;

import java.util.Objects;

public class Paciente {

    private String nombre, apellido, sexo, nombreCompaniaSeguros, historialMedico, dni;
    private int edad, numeroSeguro;

    public Paciente(String nombre, String apellido, String sexo, String nombreCompaniaSeguros, String historialMedico, int edad, String dni, int numeroSeguro) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
        this.historialMedico = historialMedico;
        this.edad = edad;
        this.dni = dni;
        this.nombreCompaniaSeguros = nombreCompaniaSeguros;
        this.numeroSeguro = numeroSeguro;
    }

    //metodos getters y setters
    public String getNombre() { return nombre; }

    public void setNombre(String nombre) { this.nombre = nombre; }

    public String getApellido() { return apellido; }

    public void setApellido(String apellido) { this.apellido = apellido; }

    public String getSexo() { return sexo; }

    public void setSexo(String sexo) { this.sexo = sexo; }

    public String getNombreCompaniaSeguros() { return nombreCompaniaSeguros; }

    public void setNombreCompaniaSeguros(String nombreCompaniaSeguros) { this.nombreCompaniaSeguros = nombreCompaniaSeguros; }

    public String getHistorialMedico() { return historialMedico; }

    public void setHistorialMedico(String historialMedico) { this.historialMedico = historialMedico; }

    public int getEdad() { return edad; }

    public void setEdad(int edad) { this.edad = edad; }

    public String getDni() { return dni; }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public int getNumeroSeguro() { return numeroSeguro; }

    public void setNumeroSeguro(int numeroSeguro) { this.numeroSeguro = numeroSeguro; }

    // registrar paciente
    public void registrar() {
        System.out.println("Paciente " + nombre + " registrado con éxito.");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Paciente paciente)) return false;
        return Objects.equals(getDni(), paciente.getDni());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDni());
    }

    public String obtenerHistorialMedico() {
        return historialMedico;
    }



}
