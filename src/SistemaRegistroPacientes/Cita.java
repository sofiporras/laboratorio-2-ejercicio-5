package SistemaRegistroPacientes;

public class Cita {
    private String idCita, idPaciente, idMedico, fechaHora;

    public Cita(String idCita, String idPaciente, String idMedico, String fechaHora) {
        this.idCita = idCita;
        this.idPaciente = idPaciente;
        this.idMedico = idMedico;
        this.fechaHora = fechaHora;
    }

    public String getIdCita() {
        return idCita;
    }

    public void setIdCita(String idCita) {
        this.idCita = idCita;
    }

    public String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(String idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(String idMedico) {
        this.idMedico = idMedico;
    }

    public String getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(String fechaHora) {
        this.fechaHora = fechaHora;
    }

    public void programarCita() {
        System.out.println("Cita programada para el paciente ID: " + idPaciente + " con el médico ID: " + idMedico + " en: " + fechaHora);
    }

    public void cancelarCita() {
        System.out.println("Cita cancelada: " + idCita);
    }

    public void reprogramarCita(String nuevaFechaHora) {
        this.fechaHora = nuevaFechaHora;
        System.out.println("Cita reprogramada para: " + nuevaFechaHora);
    }
}
