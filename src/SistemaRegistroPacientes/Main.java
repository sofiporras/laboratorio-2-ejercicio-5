package SistemaRegistroPacientes;

public class Main {
    public static void main(String[] args) {

        BaseDeDatosPacientes baseDatosPacientes = new BaseDeDatosPacientes();
        BaseDeDatosMedicos baseDatosMedicos = new BaseDeDatosMedicos();
        GestorCitas gestorCitas = new GestorCitas();


        Paciente paciente1 = new Paciente("Amelia", "Rodrigez", "Femenino", "Sura Vida", "Hirstorial medico de Amelia", 34, "1023435876", 182324);
        baseDatosPacientes.agregarPaciente(paciente1);

        Medico medico1 = new Medico("M01", "Dr. Juan", "Cardiologia", "Lunes y Jueves");
        Medico medico2 = new Medico("M02", "Dra. Angela", "Pediatria", "Martes y Viernes");

        baseDatosMedicos.agregarMedico(medico1);
        baseDatosMedicos.agregarMedico(medico2);

        Cita cita1 = new Cita("C01", "1023435876", "M01", "2024-01-25 10:00");
        Cita cita2 = new Cita("C02", "1023435876", "M02", "2024-01-26 11:00");
        gestorCitas.crearCita(cita1);
        gestorCitas.crearCita(cita2);

        Cita citaBuscada = gestorCitas.buscarCita("C01");
        if (citaBuscada != null) {
            System.out.println("Cita encontrada: " + citaBuscada.getIdCita());
        } else {
            System.out.println("Cita no encontrada");
        }

        gestorCitas.cancelarCita("C01");
        System.out.println("Cita cancelada: " + citaBuscada.getIdCita());

        if (citaBuscada != null) {
            citaBuscada.reprogramarCita("2024-01-30 10:00");
            System.out.println("Cita reprogramada: " + citaBuscada.getIdCita() + " a la nueva fecha y hora: " + citaBuscada.getFechaHora());
        }
    }
}