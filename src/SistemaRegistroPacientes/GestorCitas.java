package SistemaRegistroPacientes;

import java.util.ArrayList;
import java.util.List;

public class GestorCitas {
    private ArrayList<Cita> citas;

    public GestorCitas() {
        this.citas = new ArrayList<>();
    }

    public void crearCita(Cita cita) {
        citas.add(cita);
        System.out.println("Cita creada con éxito.");
    }

    public Cita buscarCita(String idCita) {
        for (Cita cita : citas) {
            if (cita.getIdCita().equals(idCita)) {
                return cita;
            }
        }
        return null;
    }

    public void cancelarCita(String idCita) {
        List<Cita> citasParaEliminar = new ArrayList<>();

        for (Cita cita : citas) {
            if (cita.getIdCita().equals(idCita)) {
                citasParaEliminar.add(cita);
                System.out.println("Cita cancelada: " + idCita);
            }
        }

        citas.removeAll(citasParaEliminar);
    }

}